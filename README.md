# README #

### KeepInMind Linepage ###

* O aplicativo
* O site

### O aplicativo ###

KeepInMind é um aplicativo seguidor do método contraceptivo da mulher, destinado a homens que buscam acompanhar e ajudar sua parceira.
Desenvolvido para smartphones Android, ele funcionará de forma integrada ao aparelho para lançar lembretes e notiicações de datas e estados importantes.

### O site ###

O site é uma forma de divulgar o aplicativo e entrar em contato com os usuários com o objetivo de trocar idéias para melhoria no aplicativo.